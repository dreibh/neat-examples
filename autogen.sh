#!/bin/sh -e

# Remove old configuration
rm -f CMakeCache.txt

# Configure project in current directory (.)
cmake -DCMAKE_INSTALL_PREFIX=/usr .

# Run make with available number of CPU cores
cores=`getconf _NPROCESSORS_ONLN`
make -j ${cores}
